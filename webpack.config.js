var path = require('path');
var env = process.env.NODE_ENV || 'development';

module.exports = {
  entry: './app/js/main.js',
  watch: true,
  node: {
    fs: 'empty'
  },
  output: {
    path: __dirname + '/dist',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      { test: /\.css$/, loader: 'style!css' },
      { test: /\.pug$/, loader: 'pug-loader' }
    ]
  },
  resolve: {
    alias: {
      config: path.join(__dirname, 'config', env)
    }
  }
}
