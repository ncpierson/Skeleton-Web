var gulp      = require( 'gulp' );
var sass      = require( 'gulp-sass' );
var connect   = require( 'gulp-connect' );
var pug       = require( 'gulp-pug' );
var del       = require( 'del' );
var webpack   = require( 'gulp-webpack' );

var sheets = './app/sass/*.scss';

gulp.task( 'default', [ 'serve', 'develop' ] );

gulp.task( 'serve', function () {
  return connect.server( {
    root: './dist',
    livereload: true,
    port: 4000
  } );
} );

gulp.task( 'clean', function() {
  return del( './dist' );
} );

gulp.task( 'develop', [ 'sass:watch', 'index:watch', 'webpack' ] );

gulp.task( 'build', [ 'sass', 'index', 'webpack' ] );

gulp.task( 'sass:watch', [ 'sass' ], function () {
  gulp.watch( sheets, [ 'sass' ] );
} );

gulp.task( 'sass', function () {
  return gulp.src( './app/sass/base.scss' )
    .pipe( sass().on( 'error', sass.logError) )
    .pipe( gulp.dest( './dist' ) )
    .pipe( connect.reload() );
} );

gulp.task( 'index:watch', [ 'index' ], function() {
  gulp.watch( './app/index.pug', [ 'index' ] );
} );

gulp.task( 'index', function() {
  return gulp.src( './app/index.pug' )
    .pipe( pug( {
      pretty: true
    } ) )
    .pipe( gulp.dest( './dist/' ) )
    .pipe( connect.reload() );
} );

gulp.task( 'webpack', function() {
  return gulp.src( './app/js/main.js' )
    .pipe( webpack( require( './webpack.config.js' ) ) )
    .pipe( gulp.dest( './dist' ) )
} );
