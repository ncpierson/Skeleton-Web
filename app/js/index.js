var echoTemplate = require('../templates/echo.pug');

document.addEventListener( 'DOMContentLoaded', function() {
  var messageList = document.querySelector('#messages');

  var firstMessage = document.createElement('li');
  firstMessage.innerHTML = echoTemplate({ message: 'What a message this is!' });
  messageList.appendChild(firstMessage);

  var secondMessage = document.createElement('li');
  secondMessage.innerHTML = echoTemplate({ message: 'Yes, really quite fine.' });
  messageList.appendChild(secondMessage);
} );
